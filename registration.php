<?php
/**
 * @category   FusionPWAPOS
 * @copyright  Copyright (c) 2023 Kensium Solution Pvt.Ltd. (http://www.kensiumsolutions.com/)
 */

\Magento\Framework\Component\ComponentRegistrar::register(
        \Magento\Framework\Component\ComponentRegistrar::THEME,
        'adminhtml/Fusion/PwaPosAdmin',
        __DIR__
    );
